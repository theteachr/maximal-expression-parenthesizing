# maximal-expression-parenthesizing
## The challenge
Given an expression containing simple arithmetic operations, determine the position of a pair of brackets that yields the maximum value.
```
1+2*3

1+(2*3) = (1+2*3) = 7
(1+2)*3 = 9
```
## My take
### Another way of seeing expressions
With some twisting, expressions can be seen as trees:
```
((1+2)*3)
```
can be expressed as the following tree:
```
       multiplication
      /              \
  addition            3
 /        \
1          2
```
Where leaves are values, and nodes are operations.
Let's define these types in ocaml:
```ocaml
type operator =
  | Addition
  | Substraction
  | Multiplication
```
We only define basic operations, but you will see this can easily be extended later.
```ocaml
type expression =
  | Value of int
  | Operation of operation

and operation = {
  left: expression;
  operator: operator;
  right: expression;
}
```
These definitions will make manipulating expressions easier than using plain strings.
### evaluating expressions
We want to be able to get the result of an expression.
First, we can transform the operator to a (first-class) function:
```ocaml
let function_of_operator = function
  | Addition -> ( + )
  | Multiplication -> ( * )
  | Substraction -> ( - )
```
Then use it with the result of left and right sub-expressions:
```ocaml
let rec evaluate = function
  | Value v -> v
  | Operation { left; operator; right } ->
    function_of_operator operator
      (evaluate left)
      (evaluate right)
```
### The funny part
Change in parenthesizing corresponds to a combination of a specific operation on the tree.
As an example, let's go back to our `1+2*3` expression.
Observe how a change in parenthesizing:
```
     ((1 + 2) * 3)               ------>                 (1 + (2 * 3))
```
translate in terms of tree representation:
```
       multiplication                                   addition
      /              \                                 /        \
  addition            3        can you guess?         1      multiplication    
 /        \                 -------------------->           /              \
1          2                                               2                3
```
That's right! It is a right [tree rotation](https://en.wikipedia.org/wiki/Tree_rotation)!
Given left-most parenthesized expressions, of the form:
```
           []
          /  \
        []    .
       /  \
    ...    .
    /  \
  []    .
 /  \
.    .
```
We can compute the list of all its possible parenthesizing.
Just as a little helper, we will use the `Option.bind` operator:
```ocaml
let ( let* ) = Option.bind
```
Not familiar? search for [ocaml binding operators](https://v2.ocaml.org/releases/4.11/htmlman/bindingops.html)!
We cross all possible reorderings of left and right sub-expressions, and also consider reorderings of the right-rotated expression, given it exists:
```ocaml
let rec reorders expression =
  match expression with
  | Value _ -> [ expression ]
  | Operation { left; operator; right; } ->
    List.map
      (fun left ->
         List.map
           (fun right -> Operation { left; operator; right })
           (reorders right))
      (reorders left)
    |> List.concat
    |> List.append ((let* rotated = rotate_right expression in Some (reorders rotated)) |> Option.value ~default:[])
```
Right rotation is defined as follows:
```ocaml
let rotate_right = function
  | Value _
  | Operation { left = Value _; _ } ->
    None
  | Operation {
      left = Operation { left = left'; operator = operator'; right = right' };
      operator;
      right
    } ->
      Some (Operation {
        left = left';
        operator = operator';
        right = Operation {
          left = right';
          operator;
          right
        }
      })
```
### Best parenthesizing
Now that we can compute all possible parenthesizings, and evaluate these expressions, it's almost done.
We just have to evaluate all possibilities, and choose the greatest one!
```ocaml
let process expression =
  let reorders = reorders expression in
  Printf.printf "%d reorderings:\n" (List.length reorders);
  List.iter (fun expression ->
      let value = evaluate expression in
      Printf.printf "%a = %d\n" print expression value;
    ) reorders;
  let difference e1 e2 = evaluate e2 - (evaluate e1) in
  let best = reorders
             |> List.sort difference
             |> List.hd in
  Printf.printf "---\nbest:\n%a = %d"
    print best
    (evaluate best)
```
### User input
Now, all we still  have to do is parse the user's string expression, and feed it to our `process` procedure.
 An obvious way to do this would be to use the fantastic [ocamllex/menhir](https://dev.realworldocaml.org/parsing-with-ocamllex-and-menhir.html) combo, a couple of lexer/parser generators that make it easy to write compilers in ocaml. It might seem like it would be a bit overkill for such a simple language, but it would probably still be better than a hand-crafted solution.
 Anyways.
**Let's do something dumber.**
### Regexp-based "parsing"
We can use regular expressions to transform our expression directly into valid ocaml code:
```ocaml
let expression =
  Re.(
    seq [
      group (rep1 any);
      group (alt [ char '-'; char '+'; char '*' ]);
      group (rep1 digit);
    ]
    |> compile
  )
```
(This is the equivalent of `"(.+)(-|+|* )([0-9]+)"`)
```ocaml
let rec expand expr =
  try
    Re.exec expression expr |> ignore;
    Re.(replace expression ~f expr)
  with Not_found -> "Value " ^ expr
  
and f groups =
  let get = Re.Group.get groups in
  let operator =
    match String.get (get 2) 0 with
    | '+' -> "Addition"
    | '-' -> "Substraction"
    | '*' -> "Multiplication"
    | _ -> assert false in
  let rest = get 1 in
  let left = expand rest in
  let right = get 3 in
  Printf.sprintf
    "Operation { left = %s; operator = %s; right = Value %s; }"
    left
    operator
    right
```
### Tying it all together
Our final program will read the input, expand it to ocaml expression, and output it to an ocaml file alongside the definitions and `process` it.
Then, it compiles the resulting code an runs it, thus printing the results, and giving us the long-awaited results:
```ocaml
let _ =
  print_endline "enter an expression (try 1+2*3-4)";
  let expression = expand (read_line ()) in
  let out = open_out "expression.ml" in
  Printf.fprintf out
    "let () = process (%s)"
    expression;
  close_out out;
  Sys.command "cat definitions.ml expression.ml > generated.ml && ocamlc generated.ml && ocamlrun a.out"
```
## Final note
- This project aims for clarity, and pedagogy, not efficiency. You might have bad surprises if you try it with too much terms, as the number of possibilities grows in a quadratic way, and our program simply brute-forces all of them.
- To run the code, you have to have `dune` and `re` installed, and be able to use the `Stdlib.Unix` module.
Clone the project, go into it, and run
```
dune exec ./main.exe
```
